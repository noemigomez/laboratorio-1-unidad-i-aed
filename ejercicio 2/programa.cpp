#include <iostream>
#include <cctype>
#include "Frase.h"
using namespace std;



int main(int argc, char **argv)
{
  // n para cantidad de frases
  int n;
  cout << "Cantidad de frases a escribir: ";
  cin >> n;

  Frase frases[n];
  string input;
  for(int i=-1; i<n; i++){
    /*cuando coloco getline siempre me toma la primera línea como vacía
    por eso el for comienza con un -1 que toma la linea pero no hace nada con ella
    y así se llena la lista de manera óptima*/
    if(i >= 0){
      Frase frase = Frase();
      cout << "Ingrese frase " << i + 1 << ": ";
      getline(cin, input);
      // se agrega la frase y se definen las mayúsculas y minúsculas
      frase.set_frase(input);
      frase.set_mayusculas();
      frase.set_minusculas();
      frases[i] = frase;
    }
    else{
      getline(cin, input);
    }
  }

  for(int i=0; i<n; i++){
    cout << "La frase '" << frases[i].get_frase() << "' tiene:" << endl;
    cout << "\t" << frases[i].get_mayusculas() << " mayúsculas." << endl;
    cout << "\t" << frases[i].get_minusculas() << " minúsculas." << endl;
  }
  return 0;
}
