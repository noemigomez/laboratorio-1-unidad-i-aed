# Cantidad de mayúsculas y minúsculas en frases

El programa realiza una revisión de cuántas letras mayúsculas y minúsculas hay en una cantidad definida de oraciones a ingresar. 

## Para comenzar

En el momento que comienza el programa se solicita una cantidad de frases a ingresar. Esta cantidad debe ser de tipo natural (0 o más) y se solicita luego el ingreso de esa cantidad de frases. 

Cuando se terminan de ingresar las frases el programa devuelve la frase y su respectiva cantidad tanto de mayúsculas como de minúsculas.

## Prerrequisitos

### Sistema operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make

Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando: 

`sudo apt install make`

Al tener instalado el make solo se debe ejecutar en la terminal el comando `make` dentro de la carpeta con los archivos para crear el programa

**En el caso de tener make o no querer usarlo**

Utilizar el siguiente comando dentro de la carpeta que contiene los archivos del programa:

`g++ programa.cpp Frase.cpp -o programa`

## Ejecutando las pruebas

Luego de la realización del programa (revisar Prerrequisitos) se debe ejecutar el siguiente comando en la terminal para abrir el programa:

`./programa`

Este solicitará ingresar una cantidad de frases que serán dadas posteriormente. Esta cantidad debe ser natural, si se ingresa un número negativo u otro tipo de variable, como lo puede ser una cadena de caracteres, el programa se detendrá automáticamente y retornará 0 frases.

Para las frases se pueden ingresar caracteres y números, con o sin espacios. Lo que el programa dará son cuantas mayúsculas y minúsculas tiene.

## Despliegue 

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con

- Lenguaje C++: librerías iostream, cctype.

## Autor

- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl


