#include <iostream>
#include <cctype>
using namespace std;
#include "Frase.h"

//Contructores
Frase::Frase(){
  string frase = "\0";
  int minusculas = 0 ;
  int mayusculas = 0;
}

Frase::Frase(string frase, int minusculas, int mayusculas){
  this->frase = frase;
  this->minusculas = minusculas;
  this->mayusculas = mayusculas;
}

// metodos set y get del objeto

void Frase::set_frase(string frase){
  this->frase = frase;
}

void Frase::set_mayusculas(){
  // definir frase para trabajar
  string frase = this->frase;
  // contador de mayúsculas
  int upper = 0;
  for(int i=0; i<frase.size(); i++){
    // funcion isupper retorna true si es una letra mayúscula
    if(isupper(frase[i])){
      upper++;
    }
  }
  this->mayusculas = upper;
}

void Frase::set_minusculas(){
  // definir frase para trabajar
  string frase = this->frase;
  // contador de minusculas
  int lower = 0;
  for(int i=0; i<frase.size(); i++){
    // funcion islower retorna true si es una letra minúscula
    if(islower(frase[i])){
      lower++;
    }
  }
  this->minusculas = lower;
}

string Frase::get_frase(){
  return this->frase;
}

int Frase::get_mayusculas(){
  return this->mayusculas;
}

int Frase::get_minusculas(){
  return this->minusculas;
}
