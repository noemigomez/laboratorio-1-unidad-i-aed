#include <iostream>
using namespace std;

#ifndef FRASE_H
#define FRASE_H

class Frase {
    private:
        string frase = "\0";
        int minusculas = 0 ;
        int mayusculas = 0;

    public:
        /* constructores */
        Frase();
        Frase (string frase, int minusculas, int mayusculas);

        /*metodos set y get*/
        void set_frase(string frase);
        void set_mayusculas();
        void set_minusculas();
        string get_frase();
        int get_mayusculas();
        int get_minusculas();


};
#endif
