#include <iostream>
using namespace std;

#ifndef NUMERO_H
#define NUMERO_H

class Numero {
    private:
        int cuadrado = 0;

    public:
        /* constructores */
        Numero();
        Numero (int numero);

        /*metodos set y get*/
        void set_cuadrado(int numero);
        int get_cuadrado();


};
#endif
