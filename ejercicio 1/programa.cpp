
#include <iostream>
#include "Numero.h"
using namespace std;

int main(int argc, char **argv)
{
  // el arreglo es de tamaño n ingresado por usuario
  int n;
  cout << "Ingrese tamaño de arreglo de enteros: ";
  cin >> n;

  // definir arreglos
  int arr[n];
  int arr_square[n];

  // ingreso de números enteros
  cout << "Ingrese " << n << " números:" << endl;
  for(int i=0; i<n; i++){
    int numero;
    // objeto
    Numero cuadrado = Numero();
    cout << "Ingresar " << i + 1 << " : ";
    cin >> numero;

    // declarar el cuadrado del numero ingresado como objeto
    cuadrado.set_cuadrado(numero);

    arr[i] = numero;
    arr_square[i] = cuadrado.get_cuadrado();
  }

  // cálculo de suma de los cuadrados
  int suma = 0;
  cout << "Datos de números ingresados: \n";
  for(int i=0; i<n; i++){
    // imprimir datos
    cout << "\t" << arr[i] << ", cuadrado: " << arr_square[i] << endl;
    suma = suma + arr_square[i];
  }

  cout << "LA SUMA DE LOS CUADRADOS ES: " << suma << endl;
  return 0;
}
