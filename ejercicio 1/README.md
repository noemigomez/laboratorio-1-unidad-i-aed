# Calculadora de suma de cuadrados

El programa permite ingresar una cantidad de números enteros, definidos por el usuario, y calcula la suma de los cuadrados de los números dados.

## Para comenzar

En el momento en que comienza el programa se solicita una cantidad de números a ingresar. Esta cantidad debe ser un número natural (no se pueden ingresar -1 números). 

Se señala el ingreso de N números. El programa preguntará por cada número que el usuario desee calcular su cuadrado para luego sumarlos todos; al contrario de la solicitud de cantidad, en los números para calcular su cuadrado se pueden ingresar números enteros, los que contienen a los negativos.

En ambos casos no se admiten números racionales decimales ni otro tipo de datos como cadenas de caracteres, el programa se detendrá automáticamente dando un resultado de la suma y cantidad de números 0.

## Prerrequisitos

### Sistema operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make

Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando: 

`sudo apt install make`

Al tener instalado el make solo se debe ejecutar en la terminal el comando `make` dentro de la carpeta con los archivos para crear el programa

**En el caso de tener make o no querer usarlo**

Utilizar el siguiente comando dentro de la carpeta que contiene los archivos del programa:

`g++ programa.cpp Numero.cpp -o programa`

## Ejecutando las pruebas

Luego de la realización del programa (revisar Prerrequisitos) se debe ejecutar el siguiente comando en la terminal para abrir el programa:

`./programa`

En la terminal se solicitará en primer lugar la cantidad de números que desea en el arreglo y luego le pedirá cada número entero. 

Al finalizar le entregará cada número con su respectivo cuadrado y la suma de todos estos.

## Despliegue 

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con

- Lenguaje C++: librería iostream.

## Autor

- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl

