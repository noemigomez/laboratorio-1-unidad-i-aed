# Visualización e ingreso de clientes

El programa permite a un usuario (o empresa) ingresar cierta cantidad de clientes y datos de estos tales como: nombre, teléfono, saldo y si este es moroso o no (una persona morosa es una persona que se retrasa en sus pagos o devoluciones). Posterior a su ingreso permite la visualización de cada cliente y estas mismas cualidades.

## Para comenzar

Al empezar el programa solicita la cantidad de clientes que desea ingresar.

Posterior a esto comienza a solicitar los datos de cada cliente: nombre, teléfono, saldo y si es moroso o no. En el último caso se solicita que se ingrese el caracter S para indicar que el cliente es moroso y cualquier otro caracter (1, -8, n, a, N, si, hola, $, etc) será considerado como una respuesta negativa. 

Al finalizar los ingresos de clientes el programa imprime los datos de cada cliente.

## Prerrequisitos

### Sistema operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make

Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando: 

`sudo apt install make`

Al tener instalado el make solo se debe ejecutar en la terminal el comando `make` dentro de la carpeta con los archivos para crear el programa

**En el caso de tener make o no querer usarlo**

Utilizar el siguiente comando dentro de la carpeta que contiene los archivos del programa:

`g++ programa.cpp Cliente.cpp -o programa`

## Ejecutando las pruebas

Luego de la realización del programa (revisar Prerrequisitos) se debe ejecutar el siguiente comando en la terminal para abrir el programa:

`./programa`

En primera instancia se solicita el ingreso de una cierta cantidad de clientes. Se debe de dar una cantidad natural de clientes (0 o más). Si se ingresa un número negativo o decimal se terminará automáticamente el programa.

Luego comenzará a solicitar los datos de cada cliente, según la cantidad ingresada. El nombre (se puede ingresar nombre(s) y apellido(s)), y teléfono (los teléfonos varían, se aceptará lo que se ingrese ya que es usted quien conoce los datos de los clientes). 

En el caso del saldo este debe ser un número entero natural, no existe una cantidad decimal o negativa en cuanto a cantidad de dinero en CLP (pesos chilenos), si el saldo ingresado es negativo el programa asumirá un saldo 0, mientras que el ingreso de cualquier otro tipo de saldo provocará la detención automática del programa. 

Último dentro de los datos pide ingresar si el cliente es moroso o no, para esto se utiliza el sistema de ingresar una s en afimacióón y cualquier otro caracter para la negación; esto incluye cadenas de caracteres, un caracter que no sea s, números, símbolos, etc. 

Esta solicitud de datos se repetirá para cada cliente y al finalizar imprimirá todos los datos.

## Despliegue 

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con

- Lenguaje C++: librería iostream.

## Autor

- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl




