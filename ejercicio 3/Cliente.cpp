#include <iostream>
#include "Cliente.h"
using namespace std;


//Contructores
Cliente::Cliente(){
  string nombre = "\0";
  string telefono = "\0";
  int saldo = 0;
  bool moroso;
}

Cliente::Cliente(string nombre, string telefono, int saldo, bool moroso){
  this->nombre = nombre;
  this->telefono = telefono;
  this->saldo = saldo;
  this->moroso = moroso;
}

// metodos set y get del objeto
void Cliente::set_nombre(string nombre){
  this->nombre = nombre;
}

string Cliente::get_nombre(){
  return this->nombre;
}

void Cliente::set_telefono(string telefono){
  this->telefono = telefono;
}

string Cliente::get_telefono(){
  return this->telefono;
}

void Cliente::set_saldo(int saldo){
  this->saldo = saldo;
}

int Cliente::get_saldo(){
  return this->saldo;
}

void Cliente::set_moroso(bool moroso){
  this->moroso = moroso;
}

bool Cliente::get_moroso(){
  return this->moroso;
}
