#include <iostream>
using namespace std;

#ifndef CLIENTE_H
#define CLIENTE_H

class Cliente {
    private:
        string nombre = "\0";
        string telefono = "\0";
        int saldo = 0;
        bool moroso;

    public:
        /* constructores */
        Cliente();
        Cliente (string nombre, string telefono, int saldo, bool moroso);

        /*metodos set y get*/
        void set_nombre(string nombre);
        string get_nombre();
        void set_telefono(string telefono);
        string get_telefono();
        void set_saldo(int saldo);
        int get_saldo();
        void set_moroso(bool moroso);
        bool get_moroso();


};
#endif
