#include <iostream>
#include "Cliente.h"
using namespace std;

void imprimir_cliente(int i, Cliente cliente){
  cout << "Cliente " << i + 1 << ": \n";
  cout << "\tNombre: " << cliente.get_nombre() << endl;
  cout << "\tTelefono: " << cliente.get_telefono() << endl;
  cout << "\tSaldo: $" << cliente.get_saldo() << endl;
  // retorna true o false
  if(cliente.get_moroso()){
    cout << "\tEs moroso." << endl;
  }
  else{
    cout << "\tNo es moroso." << endl;
  }
}

void leer_clientes(int n)
{
  Cliente clientes[n];
  for(int i=0; i<n; i++){
    Cliente cliente = Cliente();
    cout << "CLIENTE " << i + 1 << endl;

    string nombre, telefono;
    /*si no coloco el siguiente getline antes no me permite tomar el nombre que necesito
    siempre me quita una linea, asi que puse que lo puse para que me quitara una linea que no usaré*/
    getline(cin, nombre);
    cout << "\t Nombre: ";
    getline(cin, nombre);
    cliente.set_nombre(nombre);

    cout << "\t Telefono: ";
    cin >> telefono;
    cliente.set_telefono(telefono);

    int saldo;
    cout << "\t Saldo (en CLP): ";
    cin >> saldo;

    // no puede haber saldos negativos
    if(saldo>=0){
      cliente.set_saldo(saldo);
    }
    else{
      cout << "Saldo inválido, automático $0.\n";
      cliente.set_saldo(0);
    }

    // la respuesta y luego se define su respectivo bool
    string respuesta;
    bool moroso;
    cout << "\t ¿El cliente es moroso? (S para sí, otro para no): ";
    cin >> respuesta;

    if(respuesta == "S" || respuesta == "s"){
      moroso = true;
    }
    else{
      moroso = false;
    }
    cliente.set_moroso(moroso);
    clientes[i] = cliente;
  }

  cout << "LISTA DE CLIENTES INGRESADOS" << endl;
  for(int i=0; i<n; i++){
    imprimir_cliente(i, clientes[i]);
  }
}

int main(int argc, char **argv)
{
  // cantidad de clientes
  int n;
  cout << "Ingrese cantidad de clientes: ";
  cin >> n;

  if(n>=0){
    leer_clientes(n);
  }
  else{
    cout << "Cantidad de clientes no válida." << endl;
  }

  return 0;
}
